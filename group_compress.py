import math
import json
import random
import numpy as np
import group_lib as lib
print("Preparing dataset")
groupsize =5
friendssize  = 3
iterations = 100
no_friends_penalty = 0.5
people = lib.LoadData('people.json',friendssize)

groupcount = int(math.ceil(people.shape[0]/float(groupsize)))

best_groups = np.zeros((groupcount,groupsize))
best_error = float('inf')

print("Running")

groups =[]

names = lib.Namelist(people)
while(names.shape[0] > 0):
    person = random
    friends = lib.FindFriends(person,people)
    for friend in friends:


error = lib.GetError(groups,people,no_friends_penalty)
print(" ")
print(groups)
print(" ")
print("Done! with error: " + str(error))
