import math
import json
import random
import numpy as np
def LoadData(filename,friendssize):
    people_data= json.load(open(filename))
    people = np.zeros((len(people_data),1 + friendssize))

    for x in range(0,len(people_data)):
        people[x,0] = people_data[x]["id"]
        for y in range(0,len(people_data[x]["friends"])):
            people[x,y+1] = people_data[x]["friends"][y]["id"]
    return people
def GetError(groups,people,no_friends_penalty):
    error = 0
    #Check if no-one is forgotten
    for person in people[:,0]:
        if not person in groups[:, :]:
            error = float('inf')
            break
    #Check if friends are in same group
    for group in groups[:, :]:
        for p in group:
            d = np.where(people[:,0] == p)
            for fx in range(1,len(people[d,:][0][0]) -1):
                f = people[d,:][0][0][fx]
                if not f in group and f != 0:
                    error +=no_friends_penalty
    return error
def FindFriends(person,people):
    d = np.where(people[:,0] == person)
    friends = people[d,:][0][0]
    friends = np.delete(friends,[0],axis=0)
    return friends
def Namelist(people):
    people_remainder =np.copy(people)
    people_remainder = people_remainder[:,0]
    return people_remainder
