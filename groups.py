import math
import json
import random
import numpy as np
import group_lib as lib
print("Preparing dataset")
groupsize =5
friendssize  = 3
iterations = 100
no_friends_penalty = 0.5
people = lib.LoadData('people.json',friendssize)

groupcount = int(math.ceil(people.shape[0]/float(groupsize)))

best_groups = np.zeros((groupcount,groupsize))
best_error = float('inf')

print("Running")
for x in range(1, iterations+1):
    people_remainder = lib.Namelist(people)
    groups = np.zeros((groupcount,groupsize))
    #Random Fillment
    for group_i in range(0,groupcount):
        for person_i in range(0,groupsize):
            z= random.choice(list(enumerate(people_remainder)))
            people_remainder = np.delete(people_remainder,z[0], axis=0)
            groups[group_i][person_i] = z[1]
    error = lib.GetError(groups,people,no_friends_penalty)
    #Error compare
    if(error < best_error):
        best_groups = groups
        best_error = error
    if(error == 0):
        break;
    #Progress print
    if(x%(iterations/100) == 0):
        print(str(((float(x)/float(iterations))*100)) + " %")

print(" ")
print(best_groups)
print(" ")
print("Done! with error: " + str(best_error))
